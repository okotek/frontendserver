/*
	Currently, the livestream buffer map gets passed around on a channel. It may be better that
	a global map is used instead.
*/

package main

import (
	"encoding/gob"
	"flag"
	"fmt"
	"net"
	"net/http"
	"time"

	//"../types"
	//okolog "./okolog"
	//types "./types"
	//webDirs "./webDirs"

	loadOptions "gitlab.com/okotech/loadoptions"
	types "gitlab.com/okotek/okotypes"
	webDirs "gitlab.com/okotek/webDirs"
	"gocv.io/x/gocv"
)

//Experimental implementation of a global live frame buffer
var liveFrameStore map[string]types.LiveStoreElement
var liveFramePoint *map[string]types.LiveStoreElement = &liveFrameStore

var userTimesOrg map[string]time.Time
var userTimesPtr *map[string]time.Time = &userTimesOrg

var cfg types.ConfigOptions
var dbg bool

// Maaaaaaaaaaaiiiiiiiiiiiiinnnnnnnnnnnnnnnn----------------------------------------------------------
func main() {
	stilAddr := flag.String("serverPort", ":8080", "Port that the server is on.")
	flag.Parse()

	//okolog.LoggerIDInit(cfg)
	//cfg.InstanceIdentifier = okolog.InitStruct.Identifier

	cfg = loadOptions.Load()

	//liveFrameStore := make(chan map[string]types.LiveStoreElement)
	//userTimes := make(chan map[string]time.Time)

	liveFrameStore = make(map[string]types.LiveStoreElement)
	userTimesOrg = make(map[string]time.Time)

	//go liveFrameManager(liveFrameStore)
	go liveFrameManager()
	//go receiveLiveFrames(liveFrameStore, userTimes)
	go receiveLiveFrames()
	go StartServer(*stilAddr)

	for {

		dbg = cfg.DebugMode
		dbg = false
		time.Sleep(1000 * time.Second)
	}

}

//Keeps the channel ready to send and prevents blocking.
//TODO: Architect so that this isn't necessary
func _liveFrameManager(inputChannel chan map[string]types.LiveStoreElement) {

	fmt.Println("LFM started.")

	mapStore := make(map[string]types.LiveStoreElement)
	for {
		mapStore = <-inputChannel
		fmt.Println(len(mapStore))
		inputChannel <- mapStore
	}
}

func liveFrameManager() {
	for {

		//fmt.Print("\rlength of buffer:", len(liveFrameStore))
		time.Sleep(1 * time.Second)

		printLFS(liveFrameStore)

	}
	return
}

func printLFS(input map[string]types.LiveStoreElement) {
	for v := range input {
		tmp1 := input[v]
		for _, v2 := range tmp1 {

			fmt.Println("\n===\n", v2.UnitID, ",", v2.CamID, ",", v2.UserName, ",", v2.ImgName)

		}
	}
}

//StartServer manages the web frontend
func StartServer(serverPort string) {

	//lsBufferChan := make(chan map[string]types.LiveStoreElement)
	//var lsBuffer *map[string]types.LiveStoreElement

	root := webDirs.HandleRoot()
	lsImgDir := webDirs.LiveStreamImageHandler(liveFramePoint, userTimesPtr)
	lstreamPage := webDirs.HandleLSPage()
	serveImages := webDirs.HandleImages()
	data := webDirs.HandleMeta()
	styles := webDirs.HandleCSS()
	scriptsMain := webDirs.HandleJS("main")
	scriptsJQ := webDirs.HandleJS("JQ") //Serve up jquery
	scriptsED := webDirs.HandleJS("ED") //Serve up standard Javascript
	artwork := webDirs.HandleArt()
	//serveLiveImages := webDirs.ServeLiveImages(lsBufferChan)

	http.HandleFunc("/", root)
	http.HandleFunc("/scripts/editor.js", scriptsED)
	http.HandleFunc("/livestream", lstreamPage)
	http.HandleFunc("/data", data)
	http.HandleFunc("/img/", serveImages)
	http.HandleFunc("/lsimg/", lsImgDir)
	http.HandleFunc("/styles", styles)
	http.HandleFunc("/scripts/main.js", scriptsMain)
	http.HandleFunc("/scripts/jq.js", scriptsJQ)
	http.HandleFunc("/artwork/", artwork)

	http.ListenAndServe(serverPort, nil)
}

//For checking if livestream subprocess is recieving anything. Not for SSSSSSSSSSSSSSSSSSSSSSSW
func fillHardDrive(liveFrameStore chan map[string]types.LiveStoreElement) {

	for lfs := range liveFrameStore {
		time.Sleep(5 * time.Second)

		for name, iter := range lfs {
			for _, elem := range iter {

				img := types.CreateMatFromFrameSimp(elem)
				fmt.Println("Test code is active... DO NOT LET THIS RUN IN PRODUCTION.\n\n======\nDoing\n\n\n")
				gocv.IMWrite("/home/andrew/Dev/oko/okokit_x/"+name+".jpg", img)
			}
		}
	}
}

/*

	Listen for incoming frames and add them to the liveFrameStore buffer.

*/
func receiveLiveFrames() {
	recPort := ":8092"

	/*
		Break off a goroutine for the side channel data that is used to communicate livestream status. Listens for connection from eyeball
		process, accepts that connection and accepts the username from that eyeball process. Then, that username is checked against the lsTimeList to
		see when that user last opened the livestream page. If it was within the last couple seconds, a signal is sent back down that connection
		to let the eyeball process know to send livestream data.
	*/
	go func() {

		sideLn, sideErr := net.Listen("tcp", ":8093")
		if sideErr != nil { //Handle this better
			fmt.Println("Sidechan err: ", sideErr)
		} else {
			if dbg {
				fmt.Println("No error setting up rlf.")
			}
		}

		for {

			sideConn, scErr := sideLn.Accept() //Accept a connection from the eyeball process we're communicating with

			//This should just be userTimesOrg
			var tmpTimes map[string]time.Time = userTimesOrg //This will hold the map of usernames associated with livestream page access times

			//Break off again to allow for multiple connections in parallel.
			go func() {
				var uname string

				if scErr != nil { //TODO: handle this better
					fmt.Println("Error on sideConn accept: ", scErr)
				}

				dec := gob.NewDecoder(sideConn) //For getting data from eyeball
				enc := gob.NewEncoder(sideConn) //For sending data to eyeball

				if dbg {
					fmt.Println("Encoder and Decoder Created.")
				}

				dec.Decode(&uname) //Get the username associated with this connection
				if dbg {
					fmt.Println("Decoded username: ", uname)
				}

				//TODO: Right now, this is set to just send no matter what for testing.

				if time.Now().Unix()-tmpTimes[uname].Unix() > 2 { //Reply with information as to whether or not the user has viewed the livestream
					enc.Encode("T") //Ask eyeball for a livestream output
				} else {
					enc.Encode("F") //Do not send livestream data. Nobody is looking at it.
				}
				/*

					test := enc.Encode("T")
					if dbg {
						fmt.Println("Encoded: ", test)
					}
				*/
			}()
		}

	}()
	/*
		Now that the sidechannel has been taken care of, we set ourselves up to begin recieving and processing incoming livestream
		frame data. There is a map with usernames as the key and submaps with camera IDs as the key and frames as the value on that submap.
		This is the livestream buffer that is read by the livestream image handler and the buffer is shared via lsBufferChan. The livestream
		buffer is updated by establishing a connection to the eyeball process, associating that connection with a username, then recieving the
		frames from that connection, sorting frames into the submap by camera ID.
	*/

	var tmpLSE types.LiveStoreElement = make(map[string]types.Frame)

	ln, err := net.Listen("tcp", recPort)
	if err != nil {
		fmt.Println("Err", err)
	}

	tmpFrame := types.Frame{}

	/*
		Get the frame from the remote eyeball, add it to a tempoary map of map[string]Frame, then
		add that map to a map of map[username](that other map)

		Right now, this is single-threaded. When the time comes, make this so that there is a
		group of conns ready to go, waiting for a connection.
	*/
	for {

		conn, err := ln.Accept()
		if err != nil {
			fmt.Println("Err:", err)
		} else {
			if dbg {
				fmt.Println("Good connection for get frame.\n")
			}
		}

		dec := gob.NewDecoder(conn)
		err = dec.Decode(&tmpFrame)

		if err != nil {
			fmt.Println("Err:", err)
		} else {
			if dbg {
				fmt.Println("Decoder worked.")
			}
		}

		tmpLSE[tmpFrame.UnitID] = tmpFrame
		liveFrameStore[tmpFrame.UserName] = tmpLSE

	}

}
